package com.example.apppractica2

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView:RecyclerView=findViewById(R.id.recycler)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val albums= ArrayList<Album>()

        albums.add(Album("Imagine Dragons", 20, R.drawable.album1))
        albums.add(Album("Juanes", 26, R.drawable.album2))
        albums.add(Album("The Script", 20, R.drawable.album3))
        albums.add(Album("J Balvin", 20, R.drawable.album4))
        albums.add(Album("Nirvana", 20, R.drawable.album5))
        albums.add(Album("Jorn", 20, R.drawable.album6))
        albums.add(Album("Ruben Blades", 20, R.drawable.album7))
        albums.add(Album("Queen", 20, R.drawable.album8))

         val adapter = AdapterAlbum(albums)

         recyclerView.adapter = adapter

    }
}
