package com.example.apppractica2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class AdapterAlbum (var list: ArrayList<Album>): RecyclerView.Adapter<AdapterAlbum.ViewHolder>(){
    override fun getItemCount(): Int {
        return list.size
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v= LayoutInflater.from(parent.context).inflate(R.layout.content_item, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder:AdapterAlbum.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        fun bindItem(data:Album){
            val title:TextView= itemView.findViewById(R.id.txtTitle)
            val count:TextView= itemView.findViewById(R.id.txtCount)
            val thumbnail:ImageView= itemView.findViewById(R.id.thumbnail)

            title.text = data.name
            count.text = data.numOfSongs.toString()

            Glide.with(itemView.context).load(data.thumbnail).into(thumbnail)

            itemView.setOnClickListener{
                Toast.makeText(itemView.context, "Album de ${data.name}", Toast.LENGTH_LONG).show()
            }
        }
    }
}